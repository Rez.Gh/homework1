package animalfarm;

public class Giraffe extends Animal {
    public Giraffe(Integer age) {
        super(age);
    }

    @Override
    void eatFood() {
        System.out.println("Eet herbs ! ");
    }

    @Override
    public void moveUp(Double amount) {
        System.out.printf("This giraffe %s meter move up !%n",amount);
    }

    @Override
    public void moveDown(Double amount) {
        System.out.printf("This giraffe %s meter move down !%n",amount);
    }

    @Override
    public void moveRight(Double amount) {
        System.out.printf("This giraffe %s meter move right !%n",amount);
    }

    @Override
    public void moveLeft(Double amount) {
        System.out.printf("This giraffe %s meter move left !%n",amount);

    }

    @Override
    public void makeSound() {
        System.out.println("bleat ... bleat !");
    }
}
