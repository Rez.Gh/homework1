package animalfarm;

public interface Flyable {
    void flyHigher(Double amount);

    void flyLower(Double amount);

    void flyRight(Double amount);

    void flyLeft(Double amount);

    void flyForward(Double amount);

}
