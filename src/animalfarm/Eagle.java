package animalfarm;

public class Eagle extends FlyableBird {
    public Eagle(Integer age) {
        super(age);
    }

    @Override
    void eatFood() {
        System.out.println("Eat Rabbits");
    }

    @Override
    public void flyHigher(Double amount) {
        System.out.printf("This Eagle %s meters fly up !%n",amount);
    }

    @Override
    public void flyLower(Double amount) {
        System.out.printf("This Eagle %s meters fly down !%n",amount);
    }

    @Override
    public void flyRight(Double amount) {
        System.out.printf("This Eagle %s meters fly right !%n",amount);
    }

    @Override
    public void flyLeft(Double amount) {
        System.out.printf("This Eagle %s meters fly left !%n",amount);
    }

    @Override
    public void flyForward(Double amount) {
        System.out.printf("This Eagle %s meters fly forward !%n",amount);
    }

    @Override
    public void makeSound() {
        System.out.println("scream ... scream !");

    }
}
