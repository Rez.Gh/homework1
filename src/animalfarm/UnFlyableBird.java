package animalfarm;

public class UnFlyableBird extends Animal  {

    public UnFlyableBird(Integer age) {
        super(age);
    }

    @Override
    void eatFood() {
    }

    @Override
    void sleep() {
        super.sleep();
    }

    @Override
    public Integer getAge() {
        return super.getAge();
    }

    @Override
    public void setAge(Integer age) {
        super.setAge(age);
    }

    @Override
    public void makeSound() {
        super.makeSound();
    }

    @Override
    public void moveUp(Double amount) {

    }

    @Override
    public void moveDown(Double amount) {

    }

    @Override
    public void moveRight(Double amount) {

    }

    @Override
    public void moveLeft(Double amount) {

    }
}
