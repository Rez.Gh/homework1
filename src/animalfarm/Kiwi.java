package animalfarm;

public class Kiwi extends UnFlyableBird {

    public Kiwi(Integer age) {
        super(age);
    }

    @Override
    void eatFood() {
        System.out.println("Eet worms ! ");
    }

    @Override
    public void moveUp(Double amount) {
        System.out.printf("This Hen %s meters move up !%n",amount);
    }

    @Override
    public void moveDown(Double amount) {
        System.out.printf("This Hen %s meters move down !%n",amount);
    }

    @Override
    public void moveRight(Double amount) {
        System.out.printf("This Hen %s meters move right !%n",amount);
    }

    @Override
    public void moveLeft(Double amount) {
        System.out.printf("This Hen %s meters move left !%n",amount);

    }

    @Override
    public void makeSound() {
        System.out.println("cackle, cluck ...");
    }
}
