package animalfarm;

public class Cat extends Animal {
    public Cat(Integer age) {
        super(age);
    }

    @Override
    void eatFood() {
        System.out.println("Eat fish !");
    }

    @Override
    public void makeSound() {
        System.out.println("meow ... meow ... meow !");
    }

    @Override
    public void moveUp(Double amount) {
        System.out.printf("This cat %s meter move up !%n",amount);
    }

    @Override
    public void moveDown(Double amount) {
        System.out.printf("This cat %s meter move down !%n",amount);
    }

    @Override
    public void moveRight(Double amount) {
        System.out.printf("This cat %s meter move right !%n",amount);
    }

    @Override
    public void moveLeft(Double amount) {
        System.out.printf("This cat %s meter move left !%n",amount);

    }
}
