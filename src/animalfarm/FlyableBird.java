package animalfarm;

public class FlyableBird extends Animal implements Flyable {
    private Double beakLength;

    public FlyableBird(Integer age) {
        super(age);
    }

    @Override
    void eatFood() {
    }

    @Override
    public void moveUp(Double amount) {

    }

    @Override
    public void moveDown(Double amount) {

    }

    @Override
    public void moveRight(Double amount) {

    }

    @Override
    public void moveLeft(Double amount) {

    }

    @Override
    public void flyHigher(Double amount) {

    }

    @Override
    public void flyLower(Double amount) {

    }

    @Override
    public void flyRight(Double amount) {

    }

    @Override
    public void flyLeft(Double amount) {

    }

    @Override
    public void flyForward(Double amount) {

    }
}
