package animalfarm;

public class Main {
    public static void main(String[] args) {
        System.out.println("Welcome to Animal farm !");
        Cat cat = new Cat(3);
        Giraffe giraffe = new Giraffe(7);
        UnFlyableBird hen = new Hen(1);
        UnFlyableBird kiwi = new Kiwi(2);
        FlyableBird eagle = new Eagle(4);

        //Cat actions :
        System.out.println("Cat actions : ");
        cat.makeSound();
        cat.moveRight(5.5);
        cat.moveUp(1.0);
        cat.eatFood();
        cat.sleep();
        System.out.println("-----------------------------------");

        // Giraffe actions :
        System.out.println("Giraffe actions :");
        giraffe.eatFood();
        giraffe.moveLeft(10.0);
        giraffe.eatFood();
        giraffe.sleep();
        System.out.println("-----------------------------------");
        //Hen actions :
        System.out.println("Hen actions :");
        hen.moveLeft(5.0);
        hen.moveUp(2.2);
        hen.eatFood();
        hen.makeSound();
        hen.sleep();
        System.out.println("-----------------------------------");

        //Kiwi action :
        System.out.println("Kiwi actions : ");
        kiwi.makeSound();
        kiwi.moveRight(6.0);
        kiwi.eatFood();
        kiwi.sleep();
        System.out.println("-----------------------------------");

        //Eagle action :
        System.out.println("Eagle actions : ");
        eagle.eatFood();
        eagle.flyHigher(500.0);
        eagle.moveRight(40.5);
        eagle.makeSound();
    }
}
