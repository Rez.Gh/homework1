package toyfactory;

public class Main {
    public static void main(String[] args) {

        Car car = new Car(20000, ToySize.SMALL);
        System.out.println("Price : \n" + car.getPrice());
        System.out.println("Price after discount : \n" + car.getPrice(20));

    }
}
