package toyfactory;

public class Toy {
    private double basePrice;
    ToySize size;

    public Toy(double basePrice, ToySize size) {
        this.basePrice = basePrice;
        this.size = size;

    }

    public double getBasePrice() {
        return basePrice;
    }

    public double getPrice(double discount) {
        double discountPrice = getPrice() / (discount/100);
        return discountPrice;
    }

    public double getPrice(){
        double price = 0.0 ;
            switch (size){
                case SMALL: price= basePrice;
                    break;
                case MEDIUM: price= basePrice * 1.5;
                   break;
                case LARGE: price = basePrice * 2;
                   break;
            }
            return price;
    }
}
