package toyfactory;

public class Car extends Toy{
    public Car(double basePrice, ToySize size) {
        super(basePrice, size);
    }

    @Override
    public double getPrice() {
        double price = 0.0 ;
        switch (size){
            case SMALL: price= getBasePrice() * 2;
                break;
            case MEDIUM: price= getBasePrice() * 2.5;
                break;
            case LARGE: price = getBasePrice() * 3;
                break;
        }
        return price;
    }

    public double getPrice(double discount) {
        double discountPrice = this.getPrice() * (discount/100);
        return this.getPrice() - discountPrice;
    }
}
