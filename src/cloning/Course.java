package cloning;

public class Course implements Cloneable {
    private String subject;

    public Course(String subject) {
        this.subject = subject;
    }

    //this is method returns a shallow copy
    @Override
    public Course clone() throws CloneNotSupportedException {
        return (Course) super.clone();
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public String toString() {
        return String.format("Course name : %s%n",subject);
    }
}
