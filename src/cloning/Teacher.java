package cloning;

public class Teacher implements Cloneable {
    private String name;
    private Course course;
    private Book favouriteBook;

    public Teacher(String name, Course course, Book favouriteBook) {
        this.name = name;
        this.course = course;
        this.favouriteBook = favouriteBook;
    }

    public Book getFavouriteBook() {
        return favouriteBook;
    }

    public void setFavouriteBook(Book favouriteBook) {
        this.favouriteBook = favouriteBook;
    }

    @Override
    protected Teacher clone() throws CloneNotSupportedException {
        Book book = this.favouriteBook.clone();
        Course course = this.course.clone();
        Teacher teacher = (Teacher) super.clone();
        teacher.setFavouriteBook(book);
        teacher.setCourse(course);
        return teacher;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Override
    public String toString() {
  return String.format("Teacher name : %s%n",name) + getCourse() + getFavouriteBook();
    }
}
