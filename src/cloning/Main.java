package cloning;

public class Main {
    public static void main(String[] args) throws CloneNotSupportedException {
        System.out.println("Test Deep Cloning !");
        Teacher teacher = new Teacher("Reza",new Course("Java SE course"),
                new Book("Java book","Java","Reza"));
        Teacher secondTeacher = teacher.clone();
        // print teacher and second teacher before change course field of teacher object.

        System.out.println("Teacher object info :\n" + teacher);
        System.out.println("secondTeacher object info :\n" + secondTeacher);

        //change course field of teacher
        teacher.setCourse(new Course("Java EE"));

        // print teacher and second teacher after change course field of teacher object.
        System.out.println("------------------------------------------");
        System.out.println("After change course field of teacher object.");

        System.out.println("Teacher object info :\n" + teacher);
        System.out.println("secondTeacher object info :\n" + secondTeacher);

    }
}
