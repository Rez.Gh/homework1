package lambdas;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Enter first number : ");
        Integer firstNumber = new Scanner(System.in).nextInt();
        System.out.println("Enter Second number : ");
        Integer secondNumber = new Scanner(System.in).nextInt();

        BinaryOperator add = (f,s) -> f + s;
        BinaryOperator subtract = (f,s) -> f - s;
        BinaryOperator multiply = (f,s) -> f * s;
        BinaryOperator divide = (f,s) -> f / s ;

        System.out.println("Sum : " + add.apply(firstNumber, secondNumber));
        System.out.println("Subtraction : " + subtract.apply(firstNumber, secondNumber));
        System.out.println("Multiplication : " + multiply.apply(firstNumber, secondNumber));
        System.out.println("Division : " + divide.apply(firstNumber, secondNumber));
    }
}
