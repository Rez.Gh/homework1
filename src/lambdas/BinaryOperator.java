package lambdas;
@FunctionalInterface
public interface BinaryOperator {
    Integer apply(Integer firstNumber, Integer secondNumber);
}
